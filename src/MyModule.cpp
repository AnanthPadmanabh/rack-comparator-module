#include "plugin.hpp"


struct MyModule : Module {
	enum ParamIds {
		NUM_PARAMS
	};
	enum InputIds {
        INPUTA1,
        INPUTA2,
        INPUTB1,
        INPUTB2,
        NUM_INPUTS
	};
	enum OutputIds {
        OUTPUT1,
        OUTPUT2,
		NUM_OUTPUTS
    
	};
	enum LightIds {
        LIGHT_1,
        LIGHT_2,
		NUM_LIGHTS
	};

	MyModule() {
		config(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS);
	}

    void process(const ProcessArgs& args) override {
        if (inputs[INPUTA1].isConnected() && inputs[INPUTB1].isConnected())
        {
            float out = inputs[INPUTA1].getVoltage() >= inputs[INPUTB1].getVoltage() ? 1.0 : 0.0;
            lights[OUTPUT1].setBrightness(out);
            outputs[OUTPUT1].setVoltage(10.f * out);
        }
	}
};


struct MyModuleWidget : ModuleWidget {
	MyModuleWidget(MyModule* module) {
		setModule(module);
		setPanel(APP->window->loadSvg(asset::plugin(pluginInstance, "res/MyModule.svg")));
        
        addInput(createInputCentered<PJ301MPort>(mm2px(Vec(15.24, 46.063)), module, MyModule::INPUTA1));
        
         addInput(createInputCentered<PJ301MPort>(mm2px(Vec(15.24, 77.478)), module, MyModule::INPUTB1));
        
        addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(15.24, 108.713)), module, MyModule::OUTPUT1));
        
         addChild(createLightCentered<MediumLight<RedLight>>(mm2px(Vec(15.24, 25.81)), module, MyModule::LIGHT_1));
	}
};


Model* modelMyModule = createModel<MyModule, MyModuleWidget>("MyModule");
